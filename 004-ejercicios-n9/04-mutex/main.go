package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Numero de CPUs:", runtime.NumCPU())
	fmt.Println("Numero de Gorutinas:", runtime.NumGoroutine())

	contador := 0
	gs := 100
	var wg sync.WaitGroup
	wg.Add(gs)

	var m sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			m.Lock()
			v := contador
			v++
			contador = v
			m.Unlock()
			fmt.Println(contador)
			wg.Done()
		}()

	}
	wg.Wait()
	fmt.Println("Cuenta:", contador)

}
