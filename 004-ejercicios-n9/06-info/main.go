package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("El sistema Operativo es:", runtime.GOOS)
	fmt.Println("La arquetectura del procesador es:", runtime.GOARCH)
}
