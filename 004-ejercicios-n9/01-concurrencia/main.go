package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Printf("Numero de CPUs al inicio: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas al inicio: %v\n", runtime.NumGoroutine())
	fmt.Println("Hola que hace")
	wg.Add(2)
	go saludo()
	go saludoFormal()

	fmt.Printf("Numero de CPUs en el medio: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas en el medio: %v\n", runtime.NumGoroutine())

	fmt.Println("Casi se acaba main")

	
	wg.Wait()

	fmt.Printf("Numero de CPUs al final: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas al final: %v\n", runtime.NumGoroutine())
}

func saludo() {
	fmt.Print("nada o que hace?")
	wg.Done()
}

func saludoFormal() {
	fmt.Println("Buenas Noches Buen senhor")
	wg.Done()
}
