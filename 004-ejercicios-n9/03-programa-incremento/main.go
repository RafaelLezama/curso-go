package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Numero de CPUs:", runtime.NumCPU())
	fmt.Println("Numero de Gorutinas:", runtime.NumGoroutine())

	contador := 0
	gs := 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			v := contador
			runtime.Gosched()
			v++
			contador = v
			wg.Done()
		}()
		fmt.Println("Numero de Goroutines:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("Cuenta:", contador)

}
